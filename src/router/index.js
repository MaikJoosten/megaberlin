import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/Home';
import Map from '@/Map';
import Building from '@/Building';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '*',
      redirect: '/'
    },
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/map/:slug?/',
      name: 'map',
      component: Map
    },
    {
      path: '/building/:slug?/:showStory?',
      name: 'building',
      component: Building
    },
  ]
});
