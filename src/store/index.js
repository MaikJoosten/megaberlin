import Vue from 'vue';
import Vuex from 'vuex';
import buildings from '../../static/buildingInfos.json';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    buildings,
  },
});


export default store;
